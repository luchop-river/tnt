package com.example.android.anotadordetruco

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.ImageView
import android.widget.Toast

class MainActivity : AppCompatActivity() {

    private val MAX_PUNTOS = 30
    private val MONTICULO_FOSFOROS = 5
    private val PUNTOS_BUENAS = 15

    private lateinit var equipo1Btn: Button

    private var puntosEquipo1 : Int = 0
    private var puntosEquipo2 : Int = 0

    lateinit var imgTantos1Equipo1: ImageView
    lateinit var imgTantos2Equipo1: ImageView
    lateinit var imgTantos3Equipo1: ImageView
    lateinit var imgTantos4Equipo1: ImageView
    lateinit var imgTantos5Equipo1: ImageView
    lateinit var imgTantos6Equipo1: ImageView

    private var victoriasEquipo1 = 0
    private var victoriasEquipo2 = 0

    lateinit var btnRevancha: Button

    lateinit var btnHistorial: Button

    var historialPartidos : MutableList<String> = mutableListOf()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        equipo1Btn = findViewById(R.id.equipo1_sumar_button)
        equipo1Btn.setOnClickListener { sumarPunto() }

        imgTantos1Equipo1 = findViewById(R.id.equiop1_cuadrante1_imageView)
        imgTantos2Equipo1 = findViewById(R.id.equiop1_cuadrante2_imageView)
        imgTantos3Equipo1 = findViewById(R.id.equiop1_cuadrante3_imageView)
        imgTantos4Equipo1 = findViewById(R.id.equiop1_cuadrante4_imageView)
        imgTantos5Equipo1 = findViewById(R.id.equiop1_cuadrante5_imageView)
        imgTantos6Equipo1 = findViewById(R.id.equiop1_cuadrante6_imageView)

        btnRevancha = findViewById(R.id.revancha_button)
        btnRevancha.setOnClickListener { iniciarNuevaPartida() }

        btnHistorial = findViewById(R.id.historial_button)
        btnHistorial.setOnClickListener { verHistorial() }
    }



    private fun verHistorial() {
        val historial: ArrayList<String> = getHistorialPartidosAsArrayList()
        val intent = Intent(this, Historial::class.java)
        intent.putExtra("HISTORIAL", historial)
        startActivity(intent)
    }

    private fun getHistorialPartidosAsArrayList(): ArrayList<String> {
        return historialPartidos as ArrayList<String>
    }

    private fun iniciarNuevaPartida() {
        limpiarCuadrantes()
        puntosEquipo1 = 0
        mostrarMsgToast("Revancha! Comienza una nueva partida")
    }

    private fun limpiarCuadrantes() {
        imgTantos1Equipo1.setImageResource(R.drawable.ic_tantos_vacio)
        imgTantos2Equipo1.setImageResource(R.drawable.ic_tantos_vacio)
        imgTantos3Equipo1.setImageResource(R.drawable.ic_tantos_vacio)
        imgTantos4Equipo1.setImageResource(R.drawable.ic_tantos_vacio)
        imgTantos5Equipo1.setImageResource(R.drawable.ic_tantos_vacio)
        imgTantos6Equipo1.setImageResource(R.drawable.ic_tantos_vacio)
    }

    private fun sumarPunto( ) {
        if (sigueLaPartida()) {
            incrementarPuntaje()
            mostrarTantos()
            realizarCheckeosDelJuego()
        } else {
            mostrarMsgToast("Partida Finalizada")
        }
    }

    private fun sigueLaPartida(): Boolean {
        return (puntosEquipo1 < MAX_PUNTOS) && (puntosEquipo2 < MAX_PUNTOS)
    }

    private fun terminoLaPartida(): Boolean {
        return !sigueLaPartida()
    }

    private fun incrementarPuntaje() {
        puntosEquipo1++
    }

    private fun mostrarTantos() {
        val cuadrante: ImageView = obtenerCuadranteActual()
        mostrarTantosEnCuadrante(cuadrante)
    }

    private fun realizarCheckeosDelJuego() {
        chekearBuenas()
        checkearPartidaFinalizada()
    }

    private fun mostrarMsgToast(texto: String) {
        Toast.makeText(this, texto, Toast.LENGTH_SHORT).show()
    }


    private fun obtenerCuadranteActual(): ImageView {
        val cuadrante : ImageView = when (puntosEquipo1) {
            in 1..5 -> imgTantos1Equipo1
            in 6..10 -> imgTantos2Equipo1
            in 11..15 -> imgTantos3Equipo1
            in 16..20 -> imgTantos4Equipo1
            in 21..25 -> imgTantos5Equipo1
            else -> imgTantos6Equipo1
        }
        return cuadrante
    }

    private fun mostrarTantosEnCuadrante(cuadrante: ImageView) {
        var cantFosforos: Int = puntosEquipo1.rem(MONTICULO_FOSFOROS)

        var recursoImagen = when (cantFosforos) {
            1 -> R.drawable.ic_tantos_1
            2 -> R.drawable.ic_tantos_2
            3 -> R.drawable.ic_tantos_3
            4 -> R.drawable.ic_tantos_4
            else -> R.drawable.ic_tantos_5
        }

        cuadrante.setImageResource(recursoImagen)
    }


    private fun chekearBuenas() {
        if (this.puntosEquipo1 == PUNTOS_BUENAS) {
            mostrarMsgToast("Entraron a las buenas")
        }
    }

    private fun checkearPartidaFinalizada() {
        if (terminoLaPartida()) {
            ganoEquipo()
        }
    }

    private fun ganoEquipo() {
        agregarVictoriaEquipo()
        mostrarMsgToast("Partida Terminada!")
        agregarPartidaAlHistorial()
    }

    private fun agregarVictoriaEquipo() {
        victoriasEquipo1++
    }

    private fun agregarPartidaAlHistorial() {
        historialPartidos.add("Equipo1 - Total:${victoriasEquipo1}")
    }
}
