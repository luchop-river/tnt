package com.example.android.anotadordetruco

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView

class Historial : AppCompatActivity() {

    lateinit var historialPartidos: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_historial)

        historialPartidos = findViewById(R.id.historial_textView)

        val historial : ArrayList<String> = intent.getStringArrayListExtra("HISTORIAL")
        historialPartidos.text = historial.toString()
    }
}
