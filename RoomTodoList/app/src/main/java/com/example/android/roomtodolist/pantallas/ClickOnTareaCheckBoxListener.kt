package com.example.android.roomtodolist.pantallas

import com.example.android.roomtodolist.database.Tarea

interface ClickOnTareaCheckBoxListener {
    fun onClick(vista: Tarea, terminada: Boolean)
}