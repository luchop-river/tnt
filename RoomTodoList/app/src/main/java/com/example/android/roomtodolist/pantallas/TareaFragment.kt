package com.example.android.roomtodolist.pantallas

import android.content.Context
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.android.roomtodolist.R
import com.example.android.roomtodolist.database.Tarea
import com.example.android.roomtodolist.databinding.FragmentTareaBinding
import com.google.android.material.floatingactionbutton.FloatingActionButton


/**
 * A fragment representing a list of Items.
 * Activities containing this fragment MUST implement the
 * [TareaFragment.OnListFragmentInteractionListener] interface.
 */

class TareaFragment : Fragment() {

    //private var listener: OnListFragmentInteractionListener? = null


    lateinit var tareaViewModel: TareaViewModel
    private lateinit var recyclerView: RecyclerView


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val rootView  = inflater.inflate(R.layout.fragment_tarea_list, container, false)

        recyclerView = rootView.findViewById(R.id.list)
        val fab = rootView.findViewById<FloatingActionButton>(R.id.add_tarea_btn)


        val layoutManager = LinearLayoutManager(context)

        val adapter = context?.let { getAdapter(it) }
        recyclerView.adapter = adapter
        recyclerView.layoutManager = layoutManager


        //https://stackoverflow.com/a/53359368
        tareaViewModel = ViewModelProvider(requireActivity()).get(TareaViewModel::class.java)
        tareaViewModel.todasLasTareas.observe(viewLifecycleOwner, Observer {
                tareas -> tareas.let {
            (recyclerView.adapter as MyTareaRecyclerViewAdapter).setTareas(it) }
        })


        fab.setOnClickListener { addTarea() }

        return rootView
    }

    private fun addTarea() {
        tareaViewModel.insertar(
            Tarea(titulo = "Nueva Tarea",
                descripcion = "Agregar descripcion")
        )
    }
    private fun getAdapter(context: Context): MyTareaRecyclerViewAdapter {
        return MyTareaRecyclerViewAdapter(context,
            object:ClickOnTareaCheckBoxListener {
                override fun onClick(tarea: Tarea, terminada: Boolean) {
                    tareaViewModel.finalizarTarea(tarea, terminada)
                }
            }
        )
    }

}

