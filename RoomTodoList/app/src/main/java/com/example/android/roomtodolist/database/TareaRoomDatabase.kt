package com.example.android.roomtodolist.database

import android.content.Context
import android.util.Log
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

//@Database(entities = arrayOf(Tarea::class), version = 1, exportSchema = false)
@Database(entities = [Tarea::class], version = 1, exportSchema = false)
abstract class TareaRoomDatabase: RoomDatabase() {
    abstract fun tareaDao(): TareaDAO

    companion object {
        // Singleton previene multiples instancias de
        // la base de datos abriendose al mismo tiempo
        @Volatile
        private var INSTANCIA: TareaRoomDatabase? = null

        fun obtenerDatabase(
            context: Context,
            viewModelScope: CoroutineScope
        ): TareaRoomDatabase {
            val instanciaTemporal = INSTANCIA
            if (instanciaTemporal != null) {
                return instanciaTemporal
            }
            synchronized(this) {
                val instancia = Room.databaseBuilder(
                    context.applicationContext,
                    TareaRoomDatabase::class.java,
                    "tarea_database"
                )
                    .addCallback(TareaDatabaseCallback(viewModelScope))
                    .build()
                INSTANCIA = instancia
                return instancia
            }
        }


        private class TareaDatabaseCallback(
            private val scope: CoroutineScope
        ) : RoomDatabase.Callback() {
            /* importante: destacar el scope como parametro */

            /**
             * Lo que hacemos en esta clase es sobreescribir el método onOpen
             * para cargar la base de datos.
             *
             */
            override fun onOpen(db: SupportSQLiteDatabase) {
                super.onOpen(db)
                INSTANCIA?.let { database ->
                    scope.launch {
                        cargarBaseDeDatos(database.tareaDao())
                    }
                }
            }

            suspend fun cargarBaseDeDatos(tareaDAO: TareaDAO) {

                if(tareaDAO.cantiadadTareas() == 0) {


                    Log.i("TareaRoomDatabase", "cargarBaseDeDatos")
                    // Borrar el contenido de la base
                    tareaDAO.borrarTodas()

                    // Agregar tareas de ejemplo

                    tareaDAO.insertar(
                        Tarea(
                            titulo = "Android",
                            descripcion = "Aprender Andriod sera divertido!"
                        )
                    )

                    Log.i("TareaRoomDatabase", "tarea1 insertada con suspend")

                    tareaDAO.insertar(
                        Tarea(
                            titulo = "Aprender kotlin",
                            descripcion = "Aprender Kotlin. Va a ser divertido!"
                        )
                    )
                    Log.i("TareaRoomDatabase", "tarea2 insertada con suspend")

                } // fin comprobacion cantidad

            }
        }

    }


}