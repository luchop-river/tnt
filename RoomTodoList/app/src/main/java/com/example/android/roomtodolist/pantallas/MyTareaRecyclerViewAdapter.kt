package com.example.android.roomtodolist.pantallas

import android.content.Context
import android.util.Log
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.CompoundButton
import android.widget.TextView
import com.example.android.roomtodolist.R
import com.example.android.roomtodolist.database.Tarea


//import com.example.android.roomtodolist.pantallas.TareaFragment.OnListFragmentInteractionListener


import kotlinx.android.synthetic.main.fragment_tarea.view.*

/**
 * [RecyclerView.Adapter] that can display a [Tarea] and makes a call to the
 * specified [OnListFragmentInteractionListener].
 * TODO: Replace the implementation with code for your data type.
 */
class MyTareaRecyclerViewAdapter(
    context: Context,
    var listenerTareaCheckBox: ClickOnTareaCheckBoxListener
) : RecyclerView.Adapter<MyTareaRecyclerViewAdapter.TareaViewHolder>() {


    private var tareas = emptyList<Tarea>()



    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TareaViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.fragment_tarea, parent, false)
        return TareaViewHolder(view, listenerTareaCheckBox)
    }

    override fun onBindViewHolder(holder: TareaViewHolder, position: Int) {
        val tarea = this.tareas[position]
        holder.titulo.text = tarea.titulo
        holder.descripcion.text = tarea.descripcion
        holder.terminada.isChecked = tarea.terminada
        holder.terminada.setOnCheckedChangeListener { _: CompoundButton, checked: Boolean ->
            listenerTareaCheckBox.onClick(tarea, checked)
            //setOnClickListener {listenerTareaCheckBox.onClick(tarea, holder.terminada.isChecked)}
        }

    }


    internal fun setTareas(tareas: List<Tarea>) {
        this.tareas = tareas
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int = tareas.size

    inner class TareaViewHolder(mView: View, listener: ClickOnTareaCheckBoxListener) : RecyclerView.ViewHolder(mView) {
        var vista = mView
        var titulo: TextView
        var descripcion: TextView
        var terminada: CheckBox

        init {
            titulo = vista.tarea_titulo
            descripcion = vista.tarea_descripcion
            terminada = vista.tarea_terminada

        }

        override fun toString(): String {
            return super.toString() + " '" + titulo.text + "'"
        }
    }
}
