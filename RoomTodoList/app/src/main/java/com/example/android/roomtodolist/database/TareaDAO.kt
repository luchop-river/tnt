package com.example.android.roomtodolist.database

import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface TareaDAO {

    @Query("SELECT * FROM tabla_tarea ORDER BY tareaId ASC")
    fun getTareas(): LiveData<List<Tarea>>

    @Query("SELECT * FROM tabla_tarea WHERE tareaId = :tareaId")
    fun getTarea(tareaId: Int): LiveData<Tarea>
    /*
    @Query("SELECT tareaId FROM tabla_tarea WHERE  titulo=:titulo AND descripcion=:descripcion")
    fun getTareaID(titulo: String, descripcion: String): LiveData<Tarea>
    */


    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insertar(taera: Tarea)


    @Query("UPDATE tabla_tarea SET terminada = :terminada WHERE tareaId = :tareaId" )
    suspend fun terminarTarea(tareaId: Int, terminada: Boolean)


    @Query("DELETE FROM tabla_tarea")
    suspend fun borrarTodas()


    @Query("SELECT count(tareaId) FROM tabla_tarea")
    suspend fun cantiadadTareas(): Int

}