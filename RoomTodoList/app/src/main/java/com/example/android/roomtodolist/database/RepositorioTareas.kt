package com.example.android.roomtodolist.database

import androidx.lifecycle.LiveData

class RepositorioTareas(private val tareaDAO: TareaDAO) {
    // LiveData observada va a notificar a sus observadores cuando los datos cambien.
    val todasLasTareas: LiveData<List<Tarea>> = tareaDAO.getTareas()

    // La inserción se realiza en un hilo que no sea UI, ya que sino la aplicación se bloqueará. Para
    // informar a los métodos de llamada debemos realizar la inserción en una función suspend.
    // De esta manera, Room garantiza que no se realicen operaciones de larga ejecución
    // en el hilo principal, bloqueando la interfaz de usuario.
    suspend fun insertar(tarea: Tarea){
        tareaDAO.insertar(tarea)
    }

    suspend fun terminarTarea(tarea: Tarea, terminada: Boolean) {
        tareaDAO.terminarTarea(tarea.tareaId, terminada)
    }

}