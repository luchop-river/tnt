package com.example.android.roomtodolist.pantallas

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import com.example.android.roomtodolist.database.RepositorioTareas
import com.example.android.roomtodolist.database.Tarea
import com.example.android.roomtodolist.database.TareaRoomDatabase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class TareaViewModel(application: Application): AndroidViewModel(application)  {

    private val repositorio: RepositorioTareas

    var todasLasTareas: LiveData<List<Tarea>>

    init {
        val tareaDao = TareaRoomDatabase
            .obtenerDatabase(application, viewModelScope).tareaDao()
        repositorio = RepositorioTareas(tareaDAO = tareaDao)
        todasLasTareas = repositorio.todasLasTareas
    }

    fun insertar(tarea: Tarea) = viewModelScope.launch(Dispatchers.IO) {
        repositorio.insertar(tarea)
    }

    fun finalizarTarea(tarea: Tarea, finalizar: Boolean ) = viewModelScope.launch(Dispatchers.IO) {
        repositorio.terminarTarea(tarea, finalizar)
    }

}