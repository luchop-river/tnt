package com.example.android.roomtodolist.database

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "tabla_tarea")
data class Tarea(

    @PrimaryKey(autoGenerate = true)
    var tareaId: Int = 0,

    @ColumnInfo(name = "titulo")
    var titulo:String,

    @ColumnInfo(name = "descripcion")
    var descripcion: String,

    @ColumnInfo(name = "terminada")
    var terminada: Boolean = false

)