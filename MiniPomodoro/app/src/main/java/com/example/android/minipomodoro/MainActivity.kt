package com.example.android.minipomodoro

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.graphics.Color
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.CountDownTimer
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.databinding.DataBindingUtil
import com.example.android.minipomodoro.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    lateinit var binding: ActivityMainBinding


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)


        crearCanal(getString(R.string.pomodoro_notification_channel_id),getString(R.string.pomodoro_notification_channel_name))


        binding.botonLanzarNotificacion.setOnClickListener {
          startLongTimer("Timer Pomodoro finalizo!")
        }
    }

    private fun crearCanal(idCanal: String, nombreCanal: String) {

        // Crear canal de notificacion para versiones superiores a API 26.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val canalNotificacion = NotificationChannel(
                idCanal,
                nombreCanal,
                NotificationManager.IMPORTANCE_HIGH
            )
            .apply {
                setShowBadge(false)
                enableLights(true)
                lightColor = Color.RED
                enableVibration(true)
                description = getString(R.string.pomodoro_notification_channel_description)
            }


            // Registrar el canal
            val notificationManager = getSystemService(
                NotificationManager::class.java
            )
            notificationManager.createNotificationChannel(canalNotificacion)

        }

    }

    private fun startLongTimer(mensaje: String ) {
        startTimer(3500, mensaje)
    }


    private fun startTimer(milisDuracion: Long, mensaje: String ) {
        val timer = object: CountDownTimer(milisDuracion,1000) {

            override fun onTick(millisUntilFinished: Long) { }

            override fun onFinish() {
                notificar(mensaje)
            }
        }
        timer.start()
    }

    private fun notificar(mensaje: String) {
        val notificationManager = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            getSystemService(NotificationManager::class.java)
        } else {
            getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        }
        notificationManager.sendNotificationLong(mensaje,this)
    }

}
