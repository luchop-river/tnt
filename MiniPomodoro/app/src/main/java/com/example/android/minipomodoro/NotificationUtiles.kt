package com.example.android.minipomodoro

import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat


// Notification ID.
private val NOTIFICATION_ID = 0

/**
 * Construye y entrega una notificacion.
 *
 * @param mensaje, texto de la notificacion.
 * @param context, activity contexto.
 */
private fun NotificationManager.sendNotification(mensaje: String, applicationContext: Context, pendingIntent: PendingIntent ) {
    /*val contentIntent = Intent(applicationContext, MainActivity::class.java)
    val contentPendingIntent: PendingIntent = PendingIntent.getActivity(
        applicationContext,
        NOTIFICATION_ID,
        contentIntent,
        PendingIntent.FLAG_UPDATE_CURRENT
    )*/


    val timerUpdate = BitmapFactory.decodeResource(
        applicationContext.resources,
        R.drawable.ic_update_orange_24dp
    )
    val imgGrande = NotificationCompat.BigPictureStyle()
        .bigPicture(timerUpdate)
        .bigLargeIcon(null)

    val builder : NotificationCompat.Builder = NotificationCompat.Builder(
        applicationContext,
        applicationContext.getString(R.string.pomodoro_notification_channel_id)
    )
    .setSmallIcon(R.drawable.ic_update_orange_24dp)
    .setContentTitle(applicationContext
        .getString(R.string.titulo_notificacion))
    .setContentText(mensaje)
    .setPriority(NotificationCompat.PRIORITY_HIGH)
    .setContentIntent(pendingIntent)
    .setAutoCancel(true)
    .setLargeIcon(timerUpdate)

    with(NotificationManagerCompat.from(applicationContext)) {
      notify(NOTIFICATION_ID, builder.build())
    }
}

fun NotificationManager.sendNotificationShort(mensaje: String, applicationContext: Context) {
    val descansoIntent = Intent(applicationContext, MainActivity::class.java)
        .apply { setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP) }
    val contentDesansoIntent: PendingIntent = PendingIntent.getActivity(
        applicationContext,
        NOTIFICATION_ID,
        descansoIntent,
        PendingIntent.FLAG_CANCEL_CURRENT
    )
    sendNotification(mensaje,applicationContext,contentDesansoIntent)
}

fun NotificationManager.sendNotificationLong(mensaje: String, applicationContext: Context) {
    val contentIntent = Intent(applicationContext, DescansoActivity::class.java)
        .apply { setFlags( Intent.FLAG_ACTIVITY_NO_HISTORY ) }
    val contentPendingIntent: PendingIntent = PendingIntent.getActivity(
        applicationContext,
        NOTIFICATION_ID,
        contentIntent,
        PendingIntent.FLAG_CANCEL_CURRENT
    )
    sendNotification(mensaje, applicationContext, contentPendingIntent)
}

/**
 * Cancela todas las notificaciones.
 *
 */
fun NotificationManager.cancelarNotificaciones() {
    cancelAll()
}

