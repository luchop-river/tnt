package com.example.android.primernav

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.example.android.primernav.databinding.FragmentSegundoBinding

/**
 * A simple [Fragment] subclass.
 * Use the [segundoFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class segundoFragment : Fragment() {

    private var _binding: FragmentSegundoBinding? = null
    private val binding get() = _binding!!

    val viewModel: JuegoViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        this._binding = FragmentSegundoBinding.inflate(inflater, container, false)

        binding.juegoViewModel = viewModel
        binding.lifecycleOwner = viewLifecycleOwner

        binding.numeroText.text = viewModel.numero.value.toString()
        binding.finalAButton.setOnClickListener { onClickedFinalA() }

        return binding.root
    }


    private fun onClickedFinalA() {
        val puntajeA = viewModel.puntaje.value!!
        val action  = segundoFragmentDirections.actionSegundoFragmentToFinalAFragment(puntajeA)
        findNavController().navigate(action)
    }


}
