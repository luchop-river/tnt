package com.example.android.primernav

import android.os.Bundle
import android.renderscript.ScriptGroup
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import androidx.navigation.fragment.findNavController
import com.example.android.primernav.databinding.FragmentPrimerBinding
import kotlinx.android.synthetic.main.fragment_primer.view.*

/**
 * A simple [Fragment] subclass.
 * Use the [PrimerFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class PrimerFragment : Fragment() {


    private var _binding: FragmentPrimerBinding? = null
    private val binding get() = _binding!!


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentPrimerBinding.inflate(inflater,container,false)
        //_binding =  inflater.inflate(R.layout.fragment_primer, container, false)

        val view = binding.root

        binding.startButton.setOnClickListener{ startClicked() }

        return view
    }

    private fun startClicked() {
        findNavController().navigate(R.id.action_primerFragment_to_segundoFragment)
        //Navigation.createNavigateOnClickListener(R.id.action_primerFragment_to_segundoFragment)
    }


}
