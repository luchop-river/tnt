package com.example.android.primernav

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import kotlin.random.Random

class JuegoViewModel: ViewModel() {

    //numero
    private  var _numero = MutableLiveData<Int>(0)
    val numero: LiveData<Int>
        get() = _numero

    //puntaje
    private  var _puntaje = MutableLiveData<Int>(0)
    val puntaje: LiveData<Int>
        get() = _puntaje

    init {
        girarDados()
    }

    private fun girarDados() {
        _numero.value = Random.nextInt(0,100)
    }

    fun cuandoAdivina() {
        _puntaje.value = _puntaje.value?.plus(1)
        girarDados()
    }

    fun cuandoSaltea() {
        _puntaje.value = _puntaje.value?.minus(1)
        girarDados()
    }
}