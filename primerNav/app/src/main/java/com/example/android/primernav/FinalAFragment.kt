package com.example.android.primernav

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.example.android.primernav.databinding.FragmentFinalABinding

/**
 * A simple [Fragment] subclass.
 */
class FinalAFragment : Fragment() {

    private var _binding: FragmentFinalABinding? = null
    private val binding get() = _binding!!
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        this._binding = FragmentFinalABinding.inflate(inflater, container, false)
        val view = binding.root

        // Navegar
        binding.homeButton.setOnClickListener { navegarHaciaHome() }
        binding.volverAJugarButton.setOnClickListener { volverAPantallaJugar() }

        // Leer safe args
        val safeArgs: FinalAFragmentArgs by navArgs()
        binding.puntajeTextView.text = safeArgs.puntajeA.toString()
        return view
    }

    private fun volverAPantallaJugar() {
        findNavController().navigate(R.id.action_finalAFragment_to_segundoFragment)
    }

    private fun navegarHaciaHome() {
        findNavController().navigate(R.id.action_finalAFragment_to_primerFragment)
    }


}
