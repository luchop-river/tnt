package com.example.android.milista

import android.view.View


interface ClickListener {
    fun onClick(vista: View, position: Int )
}