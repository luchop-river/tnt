package com.example.android.milista

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView


class MainActivity : AppCompatActivity() {

    lateinit var listaRecylerView: RecyclerView
    lateinit var adapter: ProductosAdapter
    var layoutManager: RecyclerView.LayoutManager? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val producto1 = Producto("Un fosforo",100.80, "Un fosforo", R.drawable.ic_tantos_1)
        val producto2 = Producto("Tres fosforos!",199.99, "Oferta Impredible: Tres fosforos al precio de dos!!", R.drawable.ic_tantos_3)

        val listaProductos :List<Producto> = listOf<Producto>(producto1, producto2)

        listaRecylerView = findViewById(R.id.lista_RecylerView)
        layoutManager = LinearLayoutManager(this)



        adapter = ProductosAdapter(listaProductos, object:ClickListener {
            override fun onClick(vista: View, position: Int ){
                Toast.makeText(applicationContext, listaProductos[position].descripcion, Toast.LENGTH_SHORT).show()
            }
        })
        listaRecylerView.layoutManager = layoutManager
        listaRecylerView.adapter = adapter

        // TODO: hacer intent para comunicarme con otra activity
    }

}
