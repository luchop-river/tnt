package com.example.android.milista

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.producto.view.*


class ProductosAdapter (val listaProductos: List<Producto>, var clickListener: ClickListener):
    RecyclerView.Adapter<ProductosAdapter.ViewHolder>() {

    var productos: List<Producto>? = null
    var viewHolder: ViewHolder? = null

    init {
        this.productos = listaProductos
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductosAdapter.ViewHolder {
        val vista = LayoutInflater.from(parent?.context).inflate(R.layout.producto, parent, false)

        this.viewHolder = ViewHolder(vista, clickListener)

        return this.viewHolder!!
    }

    override fun getItemCount(): Int {
        return this.listaProductos.size
    }

    override fun onBindViewHolder(holder: ProductosAdapter.ViewHolder, position: Int) {
        val producto: Producto? = productos?.get(position)
        holder.nombre?.text = producto?.nombre
        holder.precio?.text = "$ ${producto?.precio}"
        holder.imagen?.setImageResource(producto?.imagen!!)

    }


    class ViewHolder(vista: View, listener: ClickListener): RecyclerView.ViewHolder(vista), View.OnClickListener{
        var vista = vista
        var imagen: ImageView? = null
        var nombre: TextView? = null
        var precio: TextView? = null

        var listener: ClickListener? = null
        init {
            imagen = vista.producto_imageView
            nombre = vista.nombre_textView
            precio = vista.precio_textView
            this.listener = listener
            this.vista.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            this.listener?.onClick(v!!, adapterPosition)
        }
    }
}

